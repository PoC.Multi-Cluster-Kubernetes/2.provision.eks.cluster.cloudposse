region = "us-west-2"

availability_zones = ["us-west-2a", "us-west-2b"]

namespace = "eg"

stage = "test"

name = "eks"

# When updating the Kubernetes version, also update the API and client-go version in test/src/go.mod
kubernetes_version = "1.22"

oidc_provider_enabled = true

enabled_cluster_log_types = ["audit"]

cluster_log_retention_period = 2

instance_types = ["t3.medium"]

desired_size = 9

max_size = 9

min_size = 9

kubernetes_labels = {}

cluster_encryption_config_enabled = true

addons = [
  {
    addon_name               = "vpc-cni"
    addon_version            = null
    resolve_conflicts        = "NONE"
    service_account_role_arn = null
  }
]
